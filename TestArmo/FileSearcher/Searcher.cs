﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileSearcher
{
    internal static class Searcher
    {

        public enum SearchStatus
        {
            None,
            SearchStarted,
            SearchEnded,
            NextFile,
            BuildTreeNodesStart,
            BuildTreeNodesNext,
            BuildTreeNodesEnd,
            Pause,
            Stop,
            Exception
        }

        public static SearchStatus Status { get; private set; } //current status of searcher

        #region Events of Searcher
        public class SearcherEventArgs : EventArgs
        {
            public int Counter { get; set; }
            public int Percentage { get; set; }
            public int MaxFiles { get; set; }
            public TimeSpan Timer { get; set; }
            public SearchStatus SearchStatus { get; set; }
            public object Value { get; set; }
            public Exception Ex { get; set; }

            public SearcherEventArgs(object value, SearchStatus status, Exception ex) { SearchStatus = status; Value = value; Ex = ex; }
        }


        public delegate void SearcherEventHandler(object sender, SearcherEventArgs e);
        public static event SearcherEventHandler SearcherEvent;
        private static void Call(SearcherEventArgs e) { SearcherEvent?.Invoke(null, e); }
        #endregion

        #region Metods Start/Stop/Play
        public static void SearchStop() { Status = SearchStatus.Stop; }

        #region  Перегрузка SearchPause - явное указание установить/снять с паузы или переключатель
        public static void SearchPause(bool Value)
        {
            if (Value && Status == SearchStatus.SearchStarted) { Status = SearchStatus.Pause; Call(new SearcherEventArgs(null, SearchStatus.Pause, null)); }
            else { Status = SearchStatus.SearchStarted; Call(new SearcherEventArgs(null, SearchStatus.SearchStarted, null)); }
        }
        private static bool paused { get; set; } // флаг переключателя
        public static void SearchPause()
        {
            if (Status == SearchStatus.SearchEnded) { return; }
            paused = !paused;
            if (paused) { Status = SearchStatus.Pause; Call(new SearcherEventArgs(null, SearchStatus.Pause, null)); }
            else { Status = SearchStatus.SearchStarted; Call(new SearcherEventArgs(null, SearchStatus.SearchStarted, null)); }
        }
        #endregion
        public static bool SearchStart(string Directory, string FileMask, string TextMask)
        {
            if (Status == SearchStatus.SearchStarted) { return false; }
            Status = SearchStatus.SearchStarted;
            paused = false;
            string[] args = new string[3] { Directory, FileMask, TextMask };
            SearcherProcessAsync(args);
            return true;
        }
        #endregion


        private static async void SearcherProcessAsync(string[] args)
        {
            List<FileInfo> result = await Task.Run(() => SearcherProcess(args));
            Call(new SearcherEventArgs(result, SearchStatus.SearchEnded, null));
            Call(new SearcherEventArgs(null, SearchStatus.BuildTreeNodesStart, null));
            TreeNode rootNode = await Task.Run(() => CreateNodes(result));
            Call(new SearcherEventArgs(rootNode, SearchStatus.BuildTreeNodesEnd, null){ Counter = result.Count});
            Status = SearchStatus.SearchEnded;
        }

        #region FileSearcher
        /// <summary>
        /// Searcher. default - SearcherProcess(), if denied folders found - recursion GetFiles()
        /// </summary>
        /// <param name="args">args[0]= Main folder; args[1] = Files mask;  args[2] = Text for search</param>
        /// <returns>Return List of suitable files</returns>
        private static List<FileInfo> SearcherProcess(object args)
        {
            string[] facts = (string[])args;
            timeForRec = DateTime.Now;
            counter = 0;
            List<FileInfo> result = null;
            string[] filesarray;

            try{filesarray = Directory.GetFiles(facts[0], facts[1], SearchOption.AllDirectories);}
            catch {filesarray = null; Call(new SearcherEventArgs(null, SearchStatus.Exception, new Exception("access denied folders found"))); }
            finally{Call(new SearcherEventArgs(null, SearchStatus.SearchStarted, null) { Percentage = 0 });}

            if (filesarray == null) { result = GetFiles(facts[0], facts[1], facts[2], new List<FileInfo>()); }
            else
            {
                result = new List<FileInfo>();
                foreach (string item in filesarray)
                {
                    while (Status == SearchStatus.Pause) { }
                    counter++;
                    Call(new SearcherEventArgs(item, SearchStatus.NextFile, null)
                    {
                        Counter = counter,
                        Timer = DateTime.Now - timeForRec,
                        MaxFiles = filesarray.Length,
                        Percentage = Convert.ToInt32((counter * 100) / filesarray.Length)
                    });
                    switch (Status)
                    {
                        case SearchStatus.SearchStarted:
                            if (string.IsNullOrEmpty(facts[2])) { result.Add(new FileInfo(item)); }
                            else
                            {
                                try
                                {
                                    string searchresult = File.ReadLines(item, System.Text.Encoding.Default).FirstOrDefault(s => s.Contains(facts[2]));
                                    if (searchresult != null) { result.Add(new FileInfo(item)); }
                                }
                                catch { }

                            }
                            break;
                        case SearchStatus.Stop: Call(new SearcherEventArgs(result, SearchStatus.Stop, null)); return result;
                    }
                }
            }
            return result;
        }

        static int counter { get; set; }
        static DateTime timeForRec { get; set; }
        public static List<FileInfo> GetFiles(string root, string searchFilesPattern, string searchTextPattern, List<FileInfo> files)
        {
            while (Status == SearchStatus.Pause) { }
            if (Status == SearchStatus.Stop) { Call(new SearcherEventArgs(files, SearchStatus.Stop, null)); return files; }
            string[] childrenfiles = null;
            try
            {
                childrenfiles = Directory.GetFiles(root, searchFilesPattern);
            }
            catch { return files; }
            foreach (string item in childrenfiles)
            {
                counter++;
                Call(new SearcherEventArgs(item, SearchStatus.NextFile, null)
                {
                    Counter = counter,
                    Timer = DateTime.Now - timeForRec,
                    MaxFiles = 0,
                    Percentage = 0
                });
                if (string.IsNullOrEmpty(searchTextPattern)) { files.Add(new FileInfo(item)); }
                else
                {
                    try
                    {
                        string searchresult = File.ReadLines(item, System.Text.Encoding.Default).FirstOrDefault(s => s.Contains(searchTextPattern));
                        if (searchresult != null) { files.Add(new FileInfo(item)); }
                    }
                    catch { }

                }
            }
            try
            {
                var childrendirs = Directory.GetDirectories(root);
                foreach (var subdir in childrendirs) { GetFiles(subdir, searchFilesPattern, searchTextPattern, files); }
            }
            catch { }



            return files;
        }


        #endregion

        #region Convert list of files to TreeNodeCollection

        //private Static async void FileListToNodes(object e)
        //{
        //    treeView1.Nodes.Clear();
        //    TreeNode rootNode = await Task.Run(() => CreateNodes(e));
        //    treeView1.Nodes.Add(rootNode);
        //    toolStripButtonStartSearch.Text = SearchStartText;
        //    Image img1 = toolStripButtonStartSearch.Image;
        //    toolStripButtonStartSearch.Image = Properties.Resources.Play_24x24;
        //    toolStripLabel3.Text = string.Empty;
        //    toolStripLabel3.Visible = false;
        //    if (img1 != null) { img1.Dispose(); img1 = null; }
        //}

        private static TreeNode CreateNodes(object e)
        {
            List<FileInfo> files = e as List<FileInfo>;
            if (files == null || files.Count ==0) { return new TreeNode("Нет найденных файлов"); }
            counter = 0;
            TreeNode rootNode = new TreeNode("Результаты поиска:");
            TreeNode lastNode = null;
            string nodeKey;
            foreach (var path in files)
            {
                counter++;
                Call(new SearcherEventArgs(null, SearchStatus.BuildTreeNodesNext, null)
                {
                    Percentage = Convert.ToInt32((counter * 100) / files.Count)
                });
                nodeKey = string.Empty;
                foreach (string subPath in path.DirectoryName.Split('\\'))
                {
                    nodeKey += subPath + '\\';
                    TreeNode[] nodes = rootNode.Nodes.Find(nodeKey, true);
                    if (nodes.Length == 0)
                    {
                        if (lastNode == null) { lastNode = rootNode.Nodes.Add(nodeKey, subPath); }
                        else { lastNode = lastNode.Nodes.Add(nodeKey, subPath); }
                    }
                    else { lastNode = nodes[0]; }
                }
                if (lastNode.Tag == null) { lastNode.Tag = new List<FileInfo>(); }
                    ((List<FileInfo>)lastNode.Tag).Add(path);
                lastNode = null;
            }
            return rootNode;
        }
        #endregion
    }
}
