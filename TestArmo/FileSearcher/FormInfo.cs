﻿using System;
using System.Windows.Forms;

namespace FileSearcher
{
    public partial class FormInfo : Form
    {
        public FormInfo()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormInfo_Load(object sender, EventArgs e)
        {
            textBox1.Text = Properties.Resources.info;
            textBox1.SelectionStart = 0;
            textBox1.SelectionLength = 0;
        }
    }
}
