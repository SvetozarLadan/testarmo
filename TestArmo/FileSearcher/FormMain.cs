﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
namespace FileSearcher
{
    public partial class FormMain : Form
    {


        #region Variables data
        private const string textBoxFolderDefault = "Выбрать папку в которой будет поиск";
        private const string textBoxFileMaskDefault = "Выбрать маску файла";
        private const string textBoxTextMaskDefault = "Выбрать текст для поиска";
        private const string SearchStartText = "Искать";
        private const string SearchStopText = "Остановить";
        private const string SearchPauseText = "Пауза";
        private const string SearchContinueText = "Продолжить";
        #endregion





        public FormMain()
        {
            InitializeComponent();

            #region Focus event subscription for textboxes 
            textBoxFolder.GotFocus += TextBoxFolder_GotFocus;
            textBoxFileMask.GotFocus += TextBoxFolder_GotFocus;
            textBoxTextMask.GotFocus += TextBoxFolder_GotFocus;

            textBoxFolder.LostFocus += TextBoxFolder_LostFocus;
            textBoxFileMask.LostFocus += TextBoxFolder_LostFocus;
            textBoxTextMask.LostFocus += TextBoxFolder_LostFocus;
            #endregion

            Searcher.SearcherEvent += Searcher_SearcherEvent; // event of searcher change state
        }

        #region Searcher event handler
        delegate void SocketRecieveDelegate(Searcher.SearcherEventArgs e);
        private void Searcher_SearcherEvent(object sender, Searcher.SearcherEventArgs e)
        {
            if (e != null) { if (InvokeRequired) { Invoke(new SocketRecieveDelegate(SearcherEventHandler), e); } else { SearcherEventHandler(e); } }
        }

        private void SearcherEventHandler(Searcher.SearcherEventArgs e)
        {
            switch (e.SearchStatus)
            {
                case Searcher.SearchStatus.NextFile:
                    toolStripLabel2.Text = $"{e.Timer.ToString("hh\\:mm\\:ss\\.fff")} мс";
                    toolStripLabel3.Text = (new FileInfo((string)e.Value)).Name;
                    if (e.MaxFiles == 0) { toolStripLabel1.Text = e.Counter.ToString(); }
                    else
                    {
                        toolStripLabel1.Text = $"{e.Counter} из {e.MaxFiles}";
                        toolStripProgressBar2.Value = e.Percentage;
                    }
                    break;
                case Searcher.SearchStatus.BuildTreeNodesNext:
                    toolStripProgressBar2.Value = e.Percentage;
                    break;
                case Searcher.SearchStatus.BuildTreeNodesStart:

                    toolStripLabel3.Text = "Построение дерева каталогов";
                    toolStripProgressBar2.Visible = true;
                    break;
                case Searcher.SearchStatus.BuildTreeNodesEnd:
                    toolStripProgressBar2.Visible = false;
                    treeView1.Nodes.Clear();
                    treeView1.Nodes.Add((TreeNode)e.Value);
                    toolStripButtonStartSearch.Text = SearchStartText;
                    Image img1 = toolStripButtonStartSearch.Image;
                    toolStripButtonStartSearch.Image = Properties.Resources.Play_24x24;
                    toolStripLabel3.Text = $"Всего найдено {e.Counter.ToString()} файлов";
                    if (img1 != null) { img1.Dispose(); img1 = null; }
                    toolStripButtonStartSearch.Enabled = true;
                    break;
                case Searcher.SearchStatus.SearchStarted:
                    toolStripProgressBar2.Value = 0;
                    toolStripSeparator1.Visible = true;
                    toolStripSeparator2.Visible = true;
                    toolStripLabel2.Visible = true;
                    toolStripSeparator3.Visible = true;
                    toolStripLabel3.Visible = true;
                    toolStripProgressBar2.Visible = true;
                    toolStripProgressBar2.Value = 0;
                    toolStripLabel4.Visible = true;
                    toolStripButton2.Visible = true;
                    Image img0 = toolStripButtonStartSearch.Image;
                    toolStripButtonStartSearch.Image = Properties.Resources.Pause_24x24;
                    toolStripButtonStartSearch.Text = SearchPauseText;
                    if (img0 != null) { img0.Dispose(); img0 = null; }
                    if (treeView1.Nodes != null) { treeView1.Nodes.Clear(); }
                    if (listBox1.Items != null) { listBox1.Items.Clear(); }
                    toolStripButtonStartSearch.Enabled = true;
                    break;
                case Searcher.SearchStatus.SearchEnded:
                    toolStripLabel1.Text = $"Предыдущий поиск: {toolStripLabel1.Text} файлов";
                    toolStripButton2.Visible = false;
                    toolStripLabel4.Visible = false;
                    break;
                case Searcher.SearchStatus.Pause:
                    Image img2 = toolStripButtonStartSearch.Image;
                    toolStripButtonStartSearch.Image = Properties.Resources.Play_24x24;
                    toolStripButtonStartSearch.Text = SearchContinueText;
                    if (img2 != null) { img2.Dispose(); img2 = null; }
                    break;
                case Searcher.SearchStatus.Exception:
                    toolStripProgressBar2.Visible = false;
                    MessageBox.Show("Обнаружены папки с запретом на чтение!" + Environment.NewLine +
                        "Во время поиска будет недоступна информация:" + Environment.NewLine +
                        "общее количество файлов, процент выполнения." + Environment.NewLine +
                        "Показываться будут только файлы подходящие по условиям.");
                    break;
            }
        }
        #endregion

        #region Focus event handlers for textboxes
        private void TextBoxFolder_GotFocus(object sender, EventArgs e)
        {
            TextBox control = (TextBox)sender;
            switch (control.Name)
            {
                case "textBoxFolder": if (control.Text.Equals(textBoxFolderDefault)) { ClearingDefaultText(control, textBoxFolderDefault); } break;
                case "textBoxFileMask": if (control.Text.Equals(textBoxFileMaskDefault)) { ClearingDefaultText(control, textBoxFileMaskDefault); } break;
                case "textBoxTextMask": if (control.Text.Equals(textBoxTextMaskDefault)) { ClearingDefaultText(control, textBoxTextMaskDefault); } break;
            }


        }
        private void TextBoxFolder_LostFocus(object sender, EventArgs e)
        {
            TextBox control = (TextBox)sender;
            if (string.IsNullOrEmpty(control.Text))
            {
                switch (control.Name)
                {
                    case "textBoxFolder": control.Text = textBoxFolderDefault; break;
                    case "textBoxFileMask": control.Text = textBoxFileMaskDefault; break;
                    case "textBoxTextMask": control.Text = textBoxTextMaskDefault; break;
                }
                control.ForeColor = Color.Gray;
            }
        }

        private void ClearingDefaultText(TextBox control, string defaulttext)
        {
            control.ForeColor = Color.Black;
            control.Text = control.Text.Replace(defaulttext, string.Empty);
            control.SelectionStart = control.Text.Length;
        }
        #endregion

        private void buttonSelectMainFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog browserDialog = new FolderBrowserDialog
            {
                Description = "Выберите папку, где будет производится поиск",
                SelectedPath = Properties.Settings.Default.MainFolderPath
            };
            DialogResult result = browserDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                textBoxFolder.Text = browserDialog.SelectedPath;
                textBoxFolder.ForeColor = Color.Black;
            }
        }

        private void toolStripButtonStartSearch_Click(object sender, EventArgs e)
        {
            switch (toolStripButtonStartSearch.Text)
            {
                case SearchStartText:
                    if (string.IsNullOrEmpty(textBoxFolder.Text) || string.IsNullOrWhiteSpace(textBoxFolder.Text)) { ShowErrorPath(); return; }
                    DirectoryInfo directory = new DirectoryInfo(textBoxFolder.Text);
                    if (directory.Exists)
                    {
                        toolStripButtonStartSearch.Enabled = false;
                        toolStripLabel1.Text = "Проверка папок на доступ";
                        toolStripLabel1.Visible = true;
                        List<FileInfo> fileList = new List<FileInfo>();
                        fileList = CreateFilesTable(textBoxFolder.Text, fileList);
                    }
                    break;
                case SearchPauseText: Searcher.SearchPause(true); break;
                case SearchContinueText: Searcher.SearchPause(false); break;
            }
        }

        private void ShowErrorPath()
        {
            MessageBox.Show($"Такая папка не найдена на диске!{Environment.NewLine}Выберите реально существующую папку.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

        private List<FileInfo> CreateFilesTable(string directory, List<FileInfo> fileList)
        {
            string filemask = "*.*";
            string textmask = string.Empty;
            if (!textBoxFileMask.Text.Equals(textBoxFileMaskDefault)) { filemask = textBoxFileMask.Text; }
            if (!textBoxTextMask.Text.Equals(textBoxTextMaskDefault)) { textmask = textBoxTextMask.Text; }
            Properties.Settings.Default.MainFolderPath = textBoxFolder.Text;
            Properties.Settings.Default.FilesMask = filemask;
            Properties.Settings.Default.TextMask = textmask;
            Properties.Settings.Default.Save();
            var r = Searcher.SearchStart(directory, filemask, textmask);
            return null;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Properties.Settings.Default.MainFolderPath)) { textBoxFolder.Text = Properties.Settings.Default.MainFolderPath; textBoxFolder.ForeColor = Color.Black; }
            if (!string.IsNullOrEmpty(Properties.Settings.Default.FilesMask) && !Properties.Settings.Default.FilesMask.Equals("*.*")) { textBoxFileMask.Text = Properties.Settings.Default.FilesMask; textBoxFileMask.ForeColor = Color.Black; }
            if (!string.IsNullOrEmpty(Properties.Settings.Default.TextMask)) { textBoxTextMask.Text = Properties.Settings.Default.TextMask; textBoxTextMask.ForeColor = Color.Black; }
            toolStripProgressBar2.Width = this.Width - 40;
            using (var frm = new FormInfo())
            {
                frm.ShowDialog();
            }
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            toolStripProgressBar2.Width = this.Width - 40;
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            toolStripButtonStartSearch.Enabled = false;
            Searcher.SearchStop();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            listBox1.Items.Clear();
            if (treeView1.SelectedNode == null || treeView1.SelectedNode.Tag == null || ((List<FileInfo>)treeView1.SelectedNode.Tag).Count == 0) { return; }
            listBox1.Items.AddRange(((List<FileInfo>)treeView1.SelectedNode.Tag).Select(x => x.Name).ToArray());
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0 || treeView1.SelectedNode == null || treeView1.SelectedNode.Tag == null) { return; }
            var file = ((List<FileInfo>)treeView1.SelectedNode.Tag).First(item => item.Name == (string)listBox1.SelectedItem);
            if (file != null)
            {
                MessageBox.Show(
                    "Полный путь: " + file.FullName + Environment.NewLine +
                    "Время изменения: " + file.LastWriteTime + Environment.NewLine +
                    "Размер: " + file.Length,
                    "Информация о файле"
                    );
            }
        }
    }
}
